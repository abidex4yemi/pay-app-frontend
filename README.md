# Web-app

## Installation

## Front-end

- Clone the repo by clicking the green clone or download button to copy the url on github
- In your terminal, run `git clone [insert URL copied from first step]`
- Open the repository with your code editor
- `develop` is the default branch and contains all full the code`
- Run `npm install` to install all dependencies
- Type `npm start` to get the development server running on the front-end

## Basic application usage

- Login details

- An admin account already exist
- Email: abidex4yemi@gmail.com
- Password: 12345678

## Required features

- Admin users can **view all of existing users**
- Admin users can **login**
- Admin users can **user can logout**
- Admin users can **Remove existing user**

## Technologies

- ExpressJS
- React
- Redux
- styled-components
- React-router-dom
