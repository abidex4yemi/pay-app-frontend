import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { GlobalStyles } from './components/atom';
import { Navbar, Footer } from './components/molecules';
import { Login } from './components/pages/Login';
import { Dashboard } from './components/pages/Dashboard';
import requiresAuth from './components/HOC/requireAuth';

const App = () => {
  return (
    <Router>
      <Navbar />
      <GlobalStyles />
      <Route exact path="/" render={(props) => <Login {...props} />} />
      <Route exact path="/dashboard" component={requiresAuth(Dashboard)} />
      <Footer />
    </Router>
  );
};

export default App;
