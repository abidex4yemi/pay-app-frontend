const validateEmail = (email) => {
  // eslint-disable-next-line no-useless-escape
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(String(email).toLowerCase());
};

const validateLoginForm = (user) => {
  let errors = {};
  const userData = Object.entries(user);

  for (let [key, value] of userData) {
    if (value.trim() === '') {
      errors[key] = `${key.toLowerCase()} is required`;
    } else {
      delete errors[key];
    }
  }

  if (!validateEmail(user.email.trim())) {
    errors.email = 'Enter a valid email address';
  } else {
    delete errors.email;
  }

  return errors;
};

export default validateLoginForm;
