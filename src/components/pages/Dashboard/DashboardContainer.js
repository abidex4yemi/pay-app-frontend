import React, { useState } from 'react';
import Dashboard from './Dashboard';

/**
 * Profile `Container component`
 * Note: container component only contains logic no `JSX`
 * this pattern of composing component allows separation of
 * logic from views
 *
 * @param {*} props
 * @returns {Object}
 */
const DashboardContainer = (props) => {
  const [showUserForm, setShowUserForm] = useState(false);

  const handleShowUserForm = () => {
    setEditUser(false);
    setShowUserForm(true);
  };

  const [userId, setUserId] = useState('');

  const [editUser, setEditUser] = useState(false);

  const handleDeleteUser = (id) => {
    const data = JSON.parse(localStorage.getItem('pay-app-data'));
    const users = data.users.filter((currentUser) => currentUser.id !== id);

    data.users = users;
    localStorage.setItem('pay-app-data', JSON.stringify(data));
    props.history.push('/dashboard');
  };

  const handleEditUser = (userId) => {
    setShowUserForm(false);
    setEditUser(true);
    setUserId(userId);
  };

  return (
    <Dashboard
      showUserForm={showUserForm}
      handleShowUserForm={handleShowUserForm}
      handleEditUser={handleEditUser}
      handleDeleteUser={handleDeleteUser}
      userId={userId}
      editUser={editUser}
    />
  );
};

export default DashboardContainer;
