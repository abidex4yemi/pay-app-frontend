import React from 'react';
import styled from 'styled-components';

import AddUser from '../Users/AddUser';
import EditUser from '../Users/EditUser';
import Users from '../Users';
import { Button } from '../../molecules/Form/Button';
import {
  white,
  secondary_color_beige,
  boxShadow,
  medium_space
} from '../../atom';

/**
 * This is a dumb component with no logic
 *
 * @param {*} props
 * @returns {object}
 */
const Dashboard = (props) => {
  const {
    handleShowUserForm,
    showUserForm,
    handleDeleteUser,
    handleEditUser
  } = props;

  const data = JSON.parse(localStorage.getItem('pay-app-data')) || {};

  const renderUsersList = () => {
    if (data.currentLoggedInUser && data.currentLoggedInUser.role === 'admin') {
      return (
        <Users
          users={data.users}
          handleDeleteUser={handleDeleteUser}
          handleEditUser={handleEditUser}
        />
      );
    }
  };

  const renderActionBar = () => {
    if (data.currentLoggedInUser && data.currentLoggedInUser.role === 'admin') {
      return <Button onClick={handleShowUserForm} buttonText="Add user" />;
    } else {
      return (
        <Button
          onClick={() => {
            const { currentLoggedInUser } = JSON.parse(
              localStorage.getItem('pay-app-data')
            );
            handleEditUser(currentLoggedInUser.id);
          }}
          buttonText="Edit profile"
        />
      );
    }
  };

  return (
    <DashboardStyled>
      <MainWrapper>
        <LeftAside>
          <ActionBar>{renderActionBar()}</ActionBar>

          {renderUsersList()}
        </LeftAside>

        <MainSection>
          {showUserForm && <AddUser />}
          {props.editUser && (
            <EditUser editUser={props.editUser} userId={props.userId} />
          )}
        </MainSection>
      </MainWrapper>
    </DashboardStyled>
  );
};

export default Dashboard;

const MainWrapper = styled.div`
  display: flex;
  justify-content: space-between;

  @media screen and (max-width: 760px) {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    min-height: calc(100vh - 100px);
  }
`;

const DashboardStyled = styled.div``;

const LeftAside = styled.aside`
  box-sizing: border-box;
  margin-top: 1px;
  background: ${secondary_color_beige};
  min-height: calc(100vh - 100px);
  width: 350px;
  box-shadow: ${boxShadow};
  padding: ${medium_space};
  overflow: auto;

  @media screen and (max-width: 760px) {
    margin: 20px 0;
    min-height: 200px;
  }

  a {
    color: ${white};
    display: block;
    padding: 5px;

    &:hover {
      color: #283e4a;
    }
  }
`;

const ActionBar = styled.div``;

const MainSection = styled.section``;
