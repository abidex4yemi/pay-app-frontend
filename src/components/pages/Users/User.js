import React from 'react';
import styled from 'styled-components';
import { Button } from '../../molecules/Form/Button';
import { boxShadow, borderRadius } from '../../atom';

/**
 * Display single user details
 *
 * @param {*} props
 */
const User = (props) => {
  const {
    id,
    email,
    firstName,
    lastName,
    role,
    handleDeleteUser,
    handleEditUser
  } = props;

  return (
    <StyledUser>
      <h3>
        Full name:
        <span>
          {firstName}-{lastName}
        </span>
      </h3>
      <p>
        Email:<span>{email}</span>
      </p>
      <p>
        Role:<span>{role}</span>
      </p>
      <ButtonGroup>
        <Button buttonText="Edit" onClick={() => handleEditUser(id)} />
        <Button buttonText="Delete" onClick={() => handleDeleteUser(id)} />
      </ButtonGroup>
    </StyledUser>
  );
};

export default User;

const StyledUser = styled.article`
  width: 300px;
  box-shadow: ${boxShadow};
  border-radius: ${borderRadius};
  padding: 2rem;
  background: #fff;
  margin-bottom: 20px;

  span {
    padding-left: 10px;
  }
`;

const ButtonGroup = styled.div`
  display: flex;
  margin: 20px 0;
  justify-content: space-between;
  color: #fff;

  button {
    width: 120px;
  }
`;
