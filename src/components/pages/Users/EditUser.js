import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';

import UserForm from '../../organisms/UserForm';
import validateUserForm from '../../../util/validateUserForm';
import styled from 'styled-components';
import { ContainerStyled } from '../../atom';

/**
 * EditUser `Container component`
 * Note: container component only contains logic no `JSX`
 * this pattern of composing component allows separation of
 * logic from views
 *
 * @param {*} props
 * @returns {Object}
 */
const EditUser = (props) => {
  const { userId } = props;

  const [user, setUser] = useState({
    id: '',
    firstName: '',
    lastName: '',
    email: '',
    role: ''
  });

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem('pay-app-data')) || {};

    const currentUser = data.users.find(
      (currentUser) => `${currentUser.id}` === userId
    );

    setUser((prevState) => ({
      ...prevState,
      ...currentUser
    }));
  }, [userId]);

  const history = useHistory();

  const [errors, setErrors] = useState({});

  const inputChange = (field, value) => {
    setUser({
      ...user,
      [field]: value
    });
  };

  const handleRadioInputChange = (evt) => {
    const { name, value } = evt.target;

    setUser({
      ...user,
      [name]: value
    });
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();
    const errors = validateUserForm(user);

    delete errors.confirmPassword;

    setErrors(() => errors);

    if (Object.values(errors).length) {
      window.scrollTo(0, 0);
      return;
    }

    const data = JSON.parse(localStorage.getItem('pay-app-data'));

    const users = data.users.map((currentUser) => {
      if (`${currentUser.id}` === `${userId}`) {
        return {
          ...currentUser,
          ...user
        };
      }

      return currentUser;
    });

    data.users = users;

    localStorage.setItem('pay-app-data', JSON.stringify(data));

    history.push('/dashboard');
  };

  return (
    <ContainerStyled>
      <MainWrapper>
        <UserForm
          errors={errors}
          user={user}
          editUser={true}
          inputChange={inputChange}
          handleSubmit={handleSubmit}
          handleRadioInputChange={handleRadioInputChange}
        />
      </MainWrapper>
    </ContainerStyled>
  );
};

export default EditUser;

const MainWrapper = styled.section`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 4rem;
`;
