import React from 'react';
import styled from 'styled-components';
import User from './User';

/**
 * @description List all users
 *
 * @param {*} props
 */
const Users = (props) => {
  const { users, handleDeleteUser, handleEditUser } = props;

  return (
    <StyledUsers>
      {users.length &&
        users.map((user) => (
          <User
            key={user.id}
            {...user}
            handleEditUser={handleEditUser}
            handleDeleteUser={handleDeleteUser}
          />
        ))}
    </StyledUsers>
  );
};

export default Users;

const StyledUsers = styled.section`
  display: flex;
  flex-direction: column;
  align-items: center;
`;
