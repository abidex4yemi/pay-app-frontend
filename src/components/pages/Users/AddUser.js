import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { useHistory } from 'react-router-dom';

import UserForm from '../../organisms/UserForm';
import validateUserForm from '../../../util/validateUserForm';
import styled from 'styled-components';
import { ContainerStyled } from '../../atom';

const bcrypt = require('bcryptjs');

/**
 * User `Container component`
 * Note: container component only contains logic no `JSX`
 * this pattern of composing component allows separation of
 * logic from views
 *
 * @param {*} props
 * @returns {Object}
 */
const AddUser = (props) => {
  const [user, setUser] = useState({
    id: uuidv4(),
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    role: 'doctor',
    status: 'active'
  });

  const history = useHistory();

  const [errors, setErrors] = useState({});

  const inputChange = (field, value) => {
    setUser({
      ...user,
      [field]: value
    });
  };

  const handleRadioInputChange = (evt) => {
    const { name, value } = evt.target;

    setUser({
      ...user,
      [name]: value
    });
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();
    const errors = validateUserForm(user);

    setErrors(() => errors);

    if (Object.values(errors).length) {
      window.scrollTo(0, 0);
      return;
    }

    const data = JSON.parse(localStorage.getItem('pay-app-data'));

    const userExist = data.users.filter(
      (currentUser) =>
        currentUser.email.toLowerCase() === user.email.toLocaleLowerCase()
    );

    if (userExist.length) {
      setErrors((prevState) => ({
        ...prevState,
        email: 'email already in use, please login to continue'
      }));

      return;
    }

    delete user.confirmPassword;

    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(user.password, salt);

    const newUserDetails = {
      ...user,
      password: hash
    };

    setUser(() => ({
      id: uuidv4(),
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      confirmPassword: '',
      role: 'doctor',
      status: 'active'
    }));

    data.users.push(newUserDetails);

    localStorage.setItem('pay-app-data', JSON.stringify(data));

    history.push('/dashboard');
  };

  return (
    <ContainerStyled>
      <MainWrapper>
        <UserForm
          errors={errors}
          user={user}
          inputChange={inputChange}
          handleSubmit={handleSubmit}
          handleRadioInputChange={handleRadioInputChange}
        />
      </MainWrapper>
    </ContainerStyled>
  );
};

export default AddUser;

const MainWrapper = styled.section`
  display: flex;
  justify-content: space-around;
  align-items: center;
  padding: 4rem;
`;
