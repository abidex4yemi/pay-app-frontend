import React, { useState, useEffect } from 'react';

import Login from './Login';
import validateLoginForm from '../../../util/validateLoginForm';
import data from '../../../data/user.json';
const bcrypt = require('bcryptjs');

/**
 * Login `Container component`
 * Note: container component only contains logic no `JSX`
 * this pattern of composing component allows separation of
 * logic from views
 *
 * @param {*} props
 * @returns {Object}
 */
const LoginContainer = (props) => {
  const loadData = () => {
    const dataExist = localStorage.getItem('pay-app-data') || {};

    if (!Object.keys(dataExist).length) {
      localStorage.setItem('pay-app-data', JSON.stringify(data));
    }
  };

  useEffect(() => {
    loadData();
  });

  const [user, setUser] = useState({
    email: '',
    password: ''
  });

  const [errors, setErrors] = useState({});

  const [invalidCredentials, setInvalidCredentials] = useState('');

  const inputChange = (field, value) => {
    setUser({
      ...user,
      [field]: value
    });
  };

  const handleSubmit = (evt) => {
    evt.preventDefault();

    const errors = validateLoginForm(user);

    setErrors(() => errors);

    if (Object.values(errors).length) {
      return;
    }

    const userExist = data.users.find(
      (currentUser) => currentUser.email === user.email
    );

    if (userExist) {
      const userPasswordValid = bcrypt.compareSync(
        user.password,
        userExist.password
      );

      if (userPasswordValid) {
        data.currentLoggedInUser = userExist;

        // save current logged in user
        localStorage.setItem('pay-app-data', JSON.stringify(data));

        props.history.push('/dashboard');
      } else {
        setInvalidCredentials('Invalid email/password');
      }
    } else {
      setInvalidCredentials('Invalid email/password');
    }
  };

  return (
    <Login
      user={user}
      errors={errors}
      inputChange={inputChange}
      loginIn={false}
      handleSubmit={handleSubmit}
      invalidCredentials={invalidCredentials}
    />
  );
};

export default LoginContainer;
