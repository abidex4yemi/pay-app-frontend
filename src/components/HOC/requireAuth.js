import React from 'react';
import { Redirect } from 'react-router-dom';

export default (Component) => {
  const RequireAuth = (props) => {
    const { currentLoggedInUser } =
      JSON.parse(localStorage.getItem('pay-app-data')) || {};

    if (currentLoggedInUser && !currentLoggedInUser.id) {
      return <Redirect to="/" />;
    }
    return <Component {...props} />;
  };

  return RequireAuth;
};
