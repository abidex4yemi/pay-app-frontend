import React from 'react';
import styled from 'styled-components';
import Loader from 'react-loader-spinner';
import {
  borderRadius,
  medium_space,
  white,
  boxShadow,
  LabelStyled,
  InputGroupStyled
} from '../atom';

import { Button, Input } from '../molecules';

/**
 * This is a dumb component with no logic
 *
 * @param {*} props
 * @returns {object}
 */
const UserForm = (props) => {
  const {
    errors,
    user,
    inputChange,
    handleSubmit,
    handleRadioInputChange
  } = props;

  const formTitle = props.editUser ? 'Edit user' : 'Add new user';

  return (
    <FormContainer>
      <h3>{formTitle}</h3>
      <form>
        <Input
          type="text"
          name="firstName"
          inputChange={inputChange}
          error={errors.firstName}
          value={user.firstName}
          labelText="First name"
        />

        <Input
          type="text"
          name="lastName"
          inputChange={inputChange}
          error={errors.lastName}
          value={user.lastName}
          labelText="Last name"
        />

        {!props.editUser && (
          <>
            <Input
              type="email"
              name="email"
              inputChange={inputChange}
              error={errors.email}
              value={user.email}
              labelText="Email"
            />

            <Input
              type="password"
              name="password"
              inputChange={inputChange}
              error={errors.password}
              value={user.password}
              labelText="Password"
            />

            <Input
              type="password"
              name="confirmPassword"
              inputChange={inputChange}
              error={errors.confirmPassword}
              value={user.confirmPassword}
              labelText="Re-Type Password"
            />
            <LabelStyled>Select user status</LabelStyled>
            <InputGroupStyled>
              <LabelStyled style={{ marginRight: '10px' }}>
                <span style={{ paddingRight: '5px' }}>Active</span>
                <input
                  type="radio"
                  name="status"
                  value="active"
                  checked={`${user.status}` === 'active'}
                  onChange={handleRadioInputChange}
                />
              </LabelStyled>

              <LabelStyled>
                <span style={{ paddingRight: '5px' }}>In-active</span>

                <input
                  type="radio"
                  name="status"
                  value="inactive"
                  checked={`${user.status}` === 'inactive'}
                  onChange={handleRadioInputChange}
                />
              </LabelStyled>
            </InputGroupStyled>
          </>
        )}

        <>
          <LabelStyled>Select user role</LabelStyled>
          <InputGroupStyled>
            <LabelStyled style={{ marginRight: '10px' }}>
              <span style={{ paddingRight: '5px' }}>Doctor</span>
              <input
                type="radio"
                name="role"
                value="doctor"
                checked={`${user.role}` === 'doctor'}
                onChange={handleRadioInputChange}
              />
            </LabelStyled>

            <LabelStyled style={{ marginRight: '10px' }}>
              <span style={{ paddingRight: '5px' }}>Accountant</span>

              <input
                type="radio"
                name="role"
                value="accountant"
                checked={`${user.role}` === 'accountant'}
                onChange={handleRadioInputChange}
              />
            </LabelStyled>

            <LabelStyled>
              <span style={{ paddingRight: '5px' }}>Admin</span>

              <input
                type="radio"
                name="role"
                value="admin"
                checked={`${user.role}` === 'admin'}
                onChange={handleRadioInputChange}
              />
            </LabelStyled>
          </InputGroupStyled>
        </>
        <Button
          buttonText={
            (props.creatingUser && (
              <Loader type="TailSpin" color="#f4f4f4" height={35} width={35} />
            )) ||
            'Submit'
          }
          onClick={handleSubmit}
          type="button"
        />
      </form>
    </FormContainer>
  );
};

export default UserForm;

const FormContainer = styled.div`
  width: 400px;
  box-shadow: ${boxShadow};
  border-radius: ${borderRadius};
  padding: 4rem;
  background: ${white};

  h3 {
    margin-bottom: ${medium_space};
    text-align: center;
  }

  button[type='submit'] {
    line-height: 20px;
  }
`;
