import React from 'react';
import styled from 'styled-components';
import { NavLink, useHistory } from 'react-router-dom';

import logo from '../../../assets/images/logo.png';
import {
  ContainerStyled,
  primary_color,
  secondary_color,
  white,
  boxShadow
} from '../../atom';

/**
 * @description Re-usable navigational component
 *
 */
export const Navbar = () => {
  const { currentLoggedInUser } =
    JSON.parse(localStorage.getItem('pay-app-data')) || {};

  const history = useHistory();

  const handleLogOut = (evt) => {
    evt.preventDefault();
    localStorage.removeItem('pay-app-data');
    history.push('/');
  };

  const renderAuthLinks = () => {
    if (currentLoggedInUser && currentLoggedInUser.id) {
      return (
        <>
          <li>
            <NavLink to="/logout" onClick={handleLogOut}>
              logout
            </NavLink>
          </li>
          <li>
            <NavLink to="/dashboard">dashboard</NavLink>
          </li>
        </>
      );
    } else {
      return (
        <>
          <li>
            <NavLink to="/">login</NavLink>
          </li>
        </>
      );
    }
  };

  return (
    <HeaderStyled>
      <ContainerStyled>
        <nav role="navigation">
          <NavLink to="/">
            <img src={logo} alt="pay logo" />
          </NavLink>

          <ul>{renderAuthLinks()}</ul>
        </nav>
      </ContainerStyled>
    </HeaderStyled>
  );
};

const HeaderStyled = styled.header`
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${white};
  background: ${primary_color}
    linear-gradient(120deg, ${secondary_color} 0, ${primary_color} 100%)
    no-repeat;
  box-shadow: ${boxShadow};

  nav {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-content: center;

    ul {
      margin: 0;
      padding: 0;
      list-style: none;

      li {
        display: inline-block;

        a {
          font-family: Verdana, Geneva, Tahoma, sans-serif;
        }
      }
    }

    img {
      width: 30px;
      height: 30px;
      max-width: 100%;
      max-height: 100%;
      background: ${white};
    }
  }
`;
