// colors
export const white = '#fff';
export const dark = '#241F21';
export const light = '#F9F9F9';
export const lightGrey = '#f5f5f5';
export const danger = '#dc3545';
export const primary_color = '#293D99';
export const secondary_color = '#245CA6';
export const secondary_color_beige = '#FAF0E3';
